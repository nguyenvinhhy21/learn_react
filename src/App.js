import React from 'react';
import { useState } from 'react';

const course = [
  {
    id:1,
    name:'HTML,CSS'
  },
  {
    id:2,
    name:'Javasrcip'
  },
  {
    id:3,
    name: 'ReactJS'
  }
]

function App() { 
  //state , props 
  const [checked,setChecked] = useState([])

  console.log(checked);

  const handSubmit = (id) =>{
    setChecked(prev => {
      const ischecked = checked.includes(id)
      if(ischecked) {
        return checked.filter(item => item !== id)
      }else{
        return [...prev, id]
      }
    })
  }

  const Submit=()=>{
    console.log({id:checked});
  }
  
    return (
      <div className ="App">
        {course.map(course =>(
           <div key={course.id}>
             <input type="checkbox"
             checked={checked.includes(course.id)} 
             onChange ={() => handSubmit(course.id)} 
             />
             {course.name}
             </div>
        ))}

     <button onClick={Submit}>An</button>
      </div>
     
    ); 
  }
export default App;
